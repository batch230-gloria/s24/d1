// console.log("WOW!");

console.log("=================== Exponent Operator =================== ");
//[SECTION] Exponent Operator

const firstNum = 8 ** 2 // ES6 update
console.log(firstNum);

const secondNum = Math.pow(8, 2); // old
console.log(secondNum);

// [SECTION] Template Literals
console.log("=================== Template Literals =================== ");
/*
	- Allows to write strings without using the concatenation operator(+)
	- Greatly helps with code reability
*/

let name = "John";
console.log("----------- Pre-template -----------")
// Pre-Template literal String
// Uses single qoute or double quote
let message = "Hello " + name + "! Welcome to programming";
console.log("Message without template literals:\n " + message);

console.log("----------- Using backticks (``) -----------")
// String Using template literals
// Uses backticks(``)     // ES6 update
message = `Hello ${name}! Welcome to Programming!`;
console.log(`Message without template literals: \n ${message}`);


const anotherMessage = `${name} attended a math competition. 
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}`;
console.log(anotherMessage);

/*
	- Template literals allows us to write strings with embedded JavaScript expressions are any valid unit of code that resolves to a value
	- "${}" are used to include JavaScript expressions in strings using template literals
*/

const interestRate = .1;
const pricincipal = 100;

console.log(`The interest of your saving account is ${pricincipal*interestRate}`);



console.log("=================== Array Destructuring =================== ");
// [SECTION] Array Destructuring
/*
	- Allows us to unpack elements in an array distinct variable
	- Allows us to name array elements with variables, instead of using index numbers

	Syntax:
		- let/const [variableName, variableName, variableName] = array;
*/

const fullName = ["Juan", "Dela", "Cruz"];

console.log("----------- Pre-Array -----------")
// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to see you`);


console.log("----------- Array Destructuring -----------")
// Array Destructuring     // ES6 update
const [firstName, middleName, lastName] = fullName
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to see you`);

console.log("================= Object Destructuring ================= ")
// [SECTION] Object Destructuring
/*
	- Allows us to unpack properties/keys of objects into distinct variables
	- Shortens the syntax for accessing properties from objects
	Syntax:
		- let/const {propertyVariableName, propertyVariableName, propertyVariableName} = object;

	Note:
		- the variableName to be assigned in destructuring should be the same as the propertyName
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

console.log("----------- Pre-Object -----------")
// Pre-Object Destructuring (using dot notation)
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again`);



console.log("----------- Object Destructuring -----------")
// Object Destructuring     // ES6 update
const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again`);




console.log("================= Arrow Functions ================= ")
// [SECTION] Arrow Functions
/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating function will not be reused in any other portion of the code
	- Adheres to "DRY" (Don't Repeat yourself) principle where there's no longer need to create a function and think of a name of function that will only be used in certain code snippet

	Syntax:
		- const variableName = () => {
			console.log("");
		}
*/
// Example: 1
const hello =()=>{
	console.log("Hello world");
}

hello();

// Example: 2
const printFullName = (firstName, middleName, lastName) =>{
	console.log(`${firstName} ${middleName} ${lastName}`);
}

printFullName("John", "D", "Smith");

const students = ["John", "Jane", "Judy"];


console.log("----------- Arrow Function in forEach -----------")
// Arrow Functions with loops
// Pre-Arrow Function
students.forEach(function(student){
	console.log(`${student} is a student.`);
})

// Arrow Function in forEach  //ES6 Update
// The function is only used in the "forEach" method to print out a text with the student's names

students.forEach((student) => {
	console.log(`${student} is a student.`)
})




console.log("============= Implicit Return Statement ============= ")
//[SECTION] Implicit Return Statement
/*
	- There are instances when you can omit the 'return' statement
	- This works because without the 'return' statement JavaScript implicity adds it for the result of the function
*/

// Pre-Arrow function
/*const add = (numA, numB) =>{
	return numA + numB;
}

let total = add(1,2);
console.log(total);*/

// ES6 update
const add = (numA, numB) => numA + numB;

let total = add(1,2);
console.log(total);




// [SECTION] Default Function Argument Value
/*
	Provides a default argument value if none is provided when the function is invoked
*/

const greet = (name = 'User') =>{
	return `Good morning, ${name}`;
}
console.log(greet("John"));
console.log(greet());



console.log("============= Class-Based Object ============= ")
// [SECTION] Class-Based Object Blueprints
/*
	- Allows creation/instantiation of onjects using classes as blueprint
	
	Syntax:
		- class className{
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA
				this.objectPropertyB = objectPropertyB
			}
		}
*/
// ES6 Update
class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);


// Instantiating an object
const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Range Raptor";
myCar.year = 2021;
console.log(myCar);
